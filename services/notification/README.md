[[_TOC_]]

# Notification
Отвечает за отправку уведомлений клиенту.

## Use case diagram
Диаграмма вариантов использования показывает, какой функционал разрабатываемой программной системы доступен каждой группе пользователей.

```plantuml
left to right direction
skinparam packageStyle rectangle

actor Delivery as delivery << Система >>

rectangle Notification {
  usecase (Отправить уведомление) as SendNotification
}

delivery --> SendNotification
```

**Use cases**
- [Отправить уведомление](use-cases/send-notification.md)

## Context diagram
Схема системного контекста является хорошей отправной точкой для построения диаграмм и документирования программной системы, позволяя вам посмотреть со стороны и увидеть общую картину. Нарисуйте схему, показывающую вашу систему в виде прямоугольника в центре, окруженного её пользователями и другими системами, с которыми она взаимодействует.

Детали здесь не важны, так как это вид, показывающий общую картину системного ландшафта. Основное внимание следует уделять людям (актерам, ролям, персонажам и т.д.) и программным системам, а не технологиям, протоколам и другим деталям низкого уровня. Это своего рода схема, которую вы могли бы показать нетехническим людям.

> Пока не готово

## Container diagram
Диаграмма контейнеров показывает высокоуровневую архитектуру программного обеспечения и то, как в ней распределяются обязанности. Она также показывает основные используемые технологии и то, как контейнеры взаимодействуют друг с другом. Это простая схема высокого уровня, ориентированная на технологии, которая одинаково полезна как для разработчиков программного обеспечения, так и для персонала службы поддержки и эксплуатации.

```plantuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml
' Components
!define actors https://gitlab.com/microarch-ru/microservices/system-design/-/raw/main/containers/actors
!define frontends https://gitlab.com/microarch-ru/microservices/system-design/-/raw/main/containers/frontends  
!define services https://gitlab.com/microarch-ru/microservices/system-design/-/raw/main/containers/services

LAYOUT_TOP_DOWN()

!include actors/courier.puml

System_Boundary(backend_boundary, "Backend") {
  !include services/delivery/ext.puml  
  System_Boundary(notification_boundary, "Notification"){
    !include services/notification/normal.puml    
  }  
}

Rel_R(delivery_ext, notification, "Статус заказа изменен", "Async, RabbitMQ")
Rel_R(notification, courier, "Уведомление", "WebSocket")
```

## Component diagram
Диаграмма компонентов показывает, из каких «компонентов» состоит контейнер, что представляет собой каждый из этих компонентов, его обязанности, технологии и детали реализации.

> Пока не готово

## Code diagram
Диаграмма классов показывает общую структуру иерархии классов системы, их коопераций, атрибутов, методов, интерфейсов и взаимосвязей между ними.

> У сервиса нет Domain Model.

## ER diagram
Диаграмма отношений сущностей это визуальное представление базы данных, которое показывает, как связаны элементы внутри. Диаграмма ER состоит из двух типов объектов — сущностей и отношений.

> У сервиса нет БД.