# UC-5 Отменить доставку

```plantuml
actor Менеджер as manager
participant "backoffice" as backoffice_web_app
participant "backoffice BFF" as backoffice_bff
participant "delivery service" as delivery_service

manager -> backoffice_web_app: Отменяет заказ
backoffice_web_app -> backoffice_bff: Отменяет заказ
note over backoffice_bff: [POST] /api/v1/deliveries/{id}/cancel

backoffice_bff -> delivery_service: Проксирует запрос
note over delivery_service: [POST] /api/v1/deliveries/{id}/cancel

backoffice_web_app <--  delivery_service: 200 OK
note over backoffice_web_app: Экран "Заказ отменен"
manager <--  backoffice_web_app: Заказ отменен
```
```