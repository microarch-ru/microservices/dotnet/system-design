# UC-2 Завершить выполнение доставки

```plantuml
actor Курьер as courier
participant "courier app" as courier_app
participant "courier BFF" as courier_bff
participant "delivery service" as delivery_service

courier -> courier_app: Завершить выполнение заказа
courier_app -> courier_bff: Завершить выполнение заказа
note over courier_bff: [POST] /api/v1/deliveries/{id}/complete

courier_bff -> delivery_service: Проксирует запрос
note over delivery_service: [POST] /api/v1/deliveries/{id}/complete

courier_app <--  delivery_service: 200 OK
note over courier_app: Сообщение "Статус заказа изменен"
courier <--  courier_app: Статус заказа изменен
```