# UC-1 Создать доставку

```plantuml
participant "basket" as basket
queue "basket_confirmed_topic" as basket_confirmed_topic
participant "delivery service" as delivery_service

basket -> basket_confirmed_topic: Cоздан новый заказ
note over basket_confirmed_topic: basket_confirmed {id,items[{id, quantity},{id, quantity}]}
basket_confirmed_topic -> delivery_service: Cоздан новый заказ
delivery_service -> delivery_service: Заказ сохранен
```