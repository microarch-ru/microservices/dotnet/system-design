# UC-4 Назначить доставку на курьера

```plantuml
participant "delivery service" as delivery_service
participant "courier app" as courier_app
actor Курьер as courier

loop
delivery_service -> delivery_service: Поиск свободного курьера
end
delivery_service -> courier_app: Назначение заказа на курьера
note over courier_app: Web Socket Message
courier_app -> courier: Уведомление курьера
```