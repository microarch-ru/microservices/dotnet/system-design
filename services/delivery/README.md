[[_TOC_]]

# Delivery
Отвечает за учет курьеров, деспетчеризацию доставкуов, доставку

## Use case diagram
Диаграмма вариантов использования показывает, какой функционал разрабатываемой программной системы доступен каждой группе пользователей.

```plantuml
left to right direction
skinparam packageStyle rectangle

actor Менеджер as manager
actor Курьер as courier
actor Basket as basket << Система >>

rectangle "Delivery" {
  usecase (Создать заказ) as UC1
  usecase (Назначить заказ на курьера) as UC2
  usecase (Переместить курьеров) as UC3
  usecase (Получить занятых курьеров) as UC6
  usecase (Получить все незавершенные заказы) as UC7
}

basket --> UC1
UC2 --> courier
UC3 --> courier 
manager --> UC6 
manager --> UC7 
```

**Use cases**
- [UC-1](use-cases/uc-1.md) Создать доставку.
- [UC-2](use-cases/uc-2.md) Завершить выполнение доставки.
- [UC-3](use-cases/uc-3.md) Получить информацию о доставке.
- [UC-4](use-cases/uc-4.md) Назначить доставку на курьера.
- [UC-5](use-cases/uc-5.md) Отменить доставку.

## Container diagram
Диаграмма контейнеров показывает высокоуровневую архитектуру программного обеспечения и то, как в ней распределяются обязанности. Она также показывает основные используемые технологии и то, как контейнеры взаимодействуют друг с другом. Это простая схема высокого уровня, ориентированная на технологии, которая одинаково полезна как для разработчиков программного обеспечения, так и для персонала службы поддержки и эксплуатации.

```plantuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml
' Components
!define actors https://gitlab.com/microarch-ru/microservices/system-design/-/raw/main/containers/actors
!define frontends https://gitlab.com/microarch-ru/microservices/system-design/-/raw/main/containers/frontends  
!define services https://gitlab.com/microarch-ru/microservices/system-design/-/raw/main/containers/services

LAYOUT_TOP_DOWN()
' LAYOUT_WITH_LEGEND()

!include actors/customer.puml
!include actors/manager.puml
!include actors/courier.puml

!include frontends/shop/web_app.puml
!include frontends/backoffice/web_app.puml
!include frontends/courier/mobile_app.puml


System_Boundary(backend_boundary, "Backend") {
  !include services/discount/ext.puml
  !include services/basket/ext.puml
  !include services/geo/ext.puml
  !include services/notification/ext.puml
  
  System_Boundary(delivery_boundary, "Delivery"){
    !include services/delivery/normal.puml
    !include services/delivery/db.puml
  }
}

Rel(customer, shop_web_app, "Получить статус доставки", "Sync, HTTPS")
Rel(manager, backoffice_web_app, "Получить статус доставки", "Sync, HTTPS")
Rel(courier, courier_mobile_app, "Изменить статус доставки", "Sync, HTTPS")

Rel(shop_web_app, delivery, "Получить статус доставки", "Sync, HTTP")
Rel(backoffice_web_app, delivery, "Получить статус доставки", "Sync, HTTP")
Rel(courier_mobile_app, delivery, "Изменить статус доставки", "Sync, HTTP")

Rel(basket_ext, discount_ext, "Получить размер скидки", "Sync, gRPC")
Rel_R(basket_ext, delivery, "Корзина оформлена", "Async, RabbitMQ")
Rel(delivery, geo_ext, "Получить геолокацию по адресу", "Sync, gRPC")
Rel(delivery, notification_ext, "Статус заказа изменен", "Async, RabbitMQ")
' Rel(notification_ext, courier_mobile_app, "Уведомление", "WebSocket")
```

## Component diagram
Диаграмма компонентов показывает, из каких «компонентов» состоит контейнер, что представляет собой каждый из этих компонентов, его обязанности, технологии и детали реализации.

```plantuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Component.puml
' LAYOUT_WITH_LEGEND()

Container(spa, "Single Page Application", "javascript and angular", "Provides all the internet banking functionality to customers via their web browser.")
Container(ma, "Mobile App", "Xamarin", "Provides a limited subset ot the internet banking functionality to customers via their mobile mobile device.")
ContainerDb(db, "Database", "Relational Database Schema", "Stores user registration information, hashed authentication credentials, access logs, etc.")
System_Ext(mbs, "Mainframe Banking System", "Stores all of the core banking information about customers, accounts, transactions, etc.")

Container_Boundary(boundary, "Warehouse") {
    Component(sign, "Sign In Controller", "MVC Rest Controlle", "Allows users to sign in to the internet banking system")
    Component(accounts, "Accounts Summary Controller", "MVC Rest Controller", "Provides customers with a summary of their bank accounts")
    Component(security, "Security Component", "Spring Bean", "Provides functionality related to singing in, changing passwords, etc.")
    Component(mbsfacade, "Mainframe Banking System Facade", "Spring Bean", "A facade onto the mainframe banking system.")

    Rel(sign, security, "Uses")
    Rel(accounts, mbsfacade, "Uses")
    Rel(security, db, "Read & write to", "JDBC")
    Rel(mbsfacade, mbs, "Uses", "XML/HTTPS")
}

Rel(spa, sign, "Uses", "JSON/HTTPS")
Rel(spa, accounts, "Uses", "JSON/HTTPS")

Rel(ma, sign, "Uses", "JSON/HTTPS")
Rel(ma, accounts, "Uses", "JSON/HTTPS")
```

## Code diagram
> Диаграмма классов показывает общую структуру иерархии классов системы, их коопераций, атрибутов, методов, интерфейсов и взаимосвязей между ними.

```plantuml
package "Warehouse Aggregate"  #DDDDDD {
  Class Warehouse <Aggregate>
  {
    - Places[] Places

    + Warehouse()
    + Place FindPlace(Good good)
    + Place GetPlaceByLocation(Location location)
    + Place GetPlaceByCategory(Category category)
    + Load(Category category, Pile pile)
    + TakeOne(Category category)
  }
  
  Class Place <Entity>
  {
    - Warehouse Warehouse
    - Location Location
    - Category Category
    - Pile item

    + Place(Warehouse warehouse, Location location, Category category)
    + SetPile(Pile pile)
  }  

  Class Pile <Value Object>{
    - Good Good
    - int Quantity
    + SubtractOne()
  }

  Class Location <Value Object> {
    - int Row
    - int Shelf 
  }

  Warehouse *- Place
  Place *- Location
  Place *-- Pile
}

package "SharedKernel" #DDDDDD {
  Class Category <Value Object>
  {
    - string Name
    + Category(string name)
  }  
  Place *- Category 

  Class Weight <Value Object>
  {
    - int Gram
    + Weight(int gram)
  }
}

package "Good Aggregate" #DDDDDD {
  Class Good <Aggregate>
  {
    - uuid Id
    - string Title
    - string Description
    - Weight Weight
    - Category Category
    + Good(Guid id, string title, string description, Weight weight, Category category)
  }
  Good *-- Weight
  Good *- Category
  Pile *-- Good
}
```

## ER diagram
> Диаграмма отношений сущностей это визуальное представление базы данных, которое показывает, как связаны элементы внутри. Диаграмма ER состоит из двух типов объектов — сущностей и отношений.

```plantuml
entity Warehouses {
  * id : uuid <<PK>>
}

entity Places {
  * id : uuid <<PK>>
  * warehouse_id <<FK>>
  * location_row : int
  * location_shelf : int
  * category_name : string
  * pile_good_id : string
  * pile_quantity : string
}

entity Goods {
  * id : uuid <<PK>>
  * title : string
  * description : description
  * category_name : string
  * weight_gram : int
}

Warehouses ||-  Places
Places }o--|| Goods
```