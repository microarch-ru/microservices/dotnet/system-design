# UC-5 Изменить остатки товара

```plantuml
participant "warehouse" as warehouse
queue "stocks" as stocks_queue
participant "catalog" as catalog

alt Успешный случай
warehouse -> stocks_queue: изменены остатки для товара
note over stocks_queue: stocks_changed {id,stocks}

stocks_queue -> catalog: изменены остатки для товара
end
```