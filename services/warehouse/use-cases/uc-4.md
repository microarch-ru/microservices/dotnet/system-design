# UC-4 Добавить новый товар

```plantuml
participant "warehouse" as warehouse
queue "products" as products_queue
participant "catalog" as catalog

alt Успешный случай
warehouse -> products_queue: добавлен новый товар
note over products_queue: new_product_added {id,name,stocks}

products_queue -> catalog: добавлен новый товар
end
```