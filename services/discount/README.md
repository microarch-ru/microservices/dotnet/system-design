[[_TOC_]]

# Discount
Отвечает за скидки, предоставляет скидку, если состав корзины подпадает под акцию.

## Use case diagram
Диаграмма вариантов использования показывает, какой функционал разрабатываемой программной системы доступен каждой группе пользователей.

```plantuml
left to right direction
skinparam packageStyle rectangle

actor Basket as basket << Система >>

rectangle Discount {
  usecase (Получить размер скидки) as GetDiscount
}

basket --> GetDiscount
```

**Use cases**
- [Получить размер скидки](use-cases/get-discount.md)

## Container diagram
Диаграмма контейнеров показывает высокоуровневую архитектуру программного обеспечения и то, как в ней распределяются обязанности. Она также показывает основные используемые технологии и то, как контейнеры взаимодействуют друг с другом. Это простая схема высокого уровня, ориентированная на технологии, которая одинаково полезна как для разработчиков программного обеспечения, так и для персонала службы поддержки и эксплуатации.

```plantuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml
' Components
!define actors https://gitlab.com/microarch-ru/microservices/system-design/-/raw/main/containers/actors
!define frontends https://gitlab.com/microarch-ru/microservices/system-design/-/raw/main/containers/frontends  
!define services https://gitlab.com/microarch-ru/microservices/system-design/-/raw/main/containers/services

LAYOUT_TOP_DOWN()
' LAYOUT_WITH_LEGEND()

!include actors/customer.puml
!include frontends/shop/web_app.puml

System_Boundary(backend_boundary, "Backend") {
  !include services/basket/ext.puml
  
  System_Boundary(discount_boundary, "Discount"){
    !include services/discount/normal.puml
    !include services/discount/db.puml
  }
}

Rel(customer, shop_web_app, "Формирует корзину, делает заказ", "Sync, HTTPS")
Rel(shop_web_app, basket_ext, "Формирует корзину, делает заказ", "HTTP")
Rel_L(basket_ext, discount, "Получить размер скидки", "Sync, gRPC")
```

<!-- ## Component diagram
Диаграмма компонентов показывает, из каких «компонентов» состоит контейнер, что представляет собой каждый из этих компонентов, его обязанности, технологии и детали реализации.

```plantuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Component.puml
' LAYOUT_WITH_LEGEND()

Container_Ext(api_client, "API Client", " HTTP REST", "Внешний потребитель API")

Container_Ext(message_bus, "Message Bus", "RabbitMQ", "Transport for business events")

Container_Boundary(basket_service, "Basket") {
    Container_Boundary(api_layer, "API Layer") {
    Component(basket_http_handler, "BasketHttpController", "Web API Controller", "Обрабатывает HTTP запросы, извлекает параметры из них")
    
    Component(goods_consumer, "GoodsConsumer", "RabbitMQ Consumer", "Обрабатывает Message из RabbitMQ")
    }

    Container_Boundary(application_layer, "Application Layer") {
      Container_Boundary(commands, "Commands") {
        Component(add_item_command, "AddItemCommand", "", "UC-1 Добавление товара в корзину")
        Component(remove_item_command, "RemoveItemCommand", "", "UC-2 Удаление товара из корзины")
        Component(add_address_command, "AddAddressCommand", "", "UC-4 Добавление адреса доставки")
        Component(checkout_command, "CheckoutCommand", "", "UC-5 Оплата заказа")
      }

      Container_Boundary(queries, "Queries") {
        Component(get_basket_query, "GetBasketQuery", "", "UC-3 Просмотр списка товаров в корзине")
      }
    }

    Container_Boundary(domain_layer, "Domain Layer") {
      Component(basket_aggregate, "Basket", "Aggregate", "Корзина товаров")
    }

    Container_Boundary(infrastructure_layer, "Infrastructure Layer") {
      Component(basket_aggregate_repository, "BasketRepository", "", "Репозиторий для сохранения/восстановления аггрегата Basket")
    }
}
ContainerDb(db, "BasketDb", "Postgres", "Хранит корзины и элементы в них")

Rel(message_bus, goods_consumer, "Добавлен новый товар/изменены остатки существющего товара", "Async, RabbitMQ")
Rel(api_client, basket_http_handler, "Добавляет, удаляет товары из корзины, оформляет ее", "HTTP")
Rel(api_layer, application_layer, "Uses")
Rel(commands, basket_aggregate, "Uses")
Rel(commands, infrastructure_layer, "Uses")
Rel(basket_aggregate_repository, db, "Uses")
Rel(infrastructure_layer, basket_aggregate, "Uses")
Rel(get_basket_query, db, "Uses")

Lay_U(domain_layer, infrastructure_layer)
``` -->

## Code diagram
Диаграмма классов показывает общую структуру иерархии классов системы, их коопераций, атрибутов, методов, интерфейсов и взаимосвязей между ними.

```plantuml
package "Basket Aggregate"  #DDDDDD {
  Class Basket <Aggregate>
  {
    - uuid Id
    - uuid BuyerId
    - Address Address
    - BankCardDetails BankCardDetails
    - TimeSlot TimeSlot
    - Item[] Items
    - Status Status

    - Basket()
    - Basket(Guid buyerId)
    + Result<Basket, Error> Create(Guid buyerId)
    + Result<object, Error> Change(Good good, int quantity)
    + Result<object, Error> Clear()
    + Result<object, Error> AddAddress(Address address)
    + Result<object, Error> AddBankCard(BankCardDetails bankCardDetails)
    + Result<object, Error> AddTimeSlot(TimeSlot timeSlot)
    + Result<object, Error> Checkout()
  }

  Class Item <Entity>
  {
    - uuid Id
    - string Title
    - string Description
    - int Price
    - int Quantity
    - Item()    
    - Item(Good good, int quantity)
    - Item(Good good, int quantity)
    + Result<Item, Error> Create(Good good, int quantity)
    + Result<object, Error> SetQuantity(int quantity)
  }  

  Class Address <Value Object>{
    - string Country
    - string City
    - string Street
    - string House
    - string Apartment

    - Address()
    - Address(string country, string city, string street, string house, string apartment)
    + Result<Address, Error> Create(string country, string city, string street, string house, string apartment)
  }

  Class Status <Entity>{
    - int Id
    - string Name
    - int Start
    - int End
    - TimeSlot()
    - TimeSlot(int id, string name, int start, int end)
    + Status FromName(string name)
    + Status From(int id)    
  }

  Class TimeSlot <Entity>{
    - int Id
    - string Name
    - Status()
    - Status(int id, string name)
    + Status FromName(string name)
    + Status From(int id)    
  }  
}

package "Good Aggregate"  #DDDDDD {
  Class Good <Aggregate>
  {
    - uuid Id
    - string Title
    - string Description
    - int Price
    - Good()
    - Good(Guid id, string title, string description, int price)
    + Result<Good, Error> Create(string title, string description, int price)
  }

Basket *-- Item
Basket *- Address
Basket *-- Status
Basket *-- TimeSlot

Item *-- Good
```
## ER diagram
Диаграмма отношений сущностей это визуальное представление базы данных, которое показывает, как связаны элементы внутри. Диаграмма ER состоит из двух типов объектов — сущностей и отношений.

```plantuml
entity goods {
  * id : uuid <<PK>>
  * category_name : string
  * description : string
  * title : string
  * weight_gram : integer
}

entity baskets {
  * id : uuid <<PK>>
  * buyer_id : uuid
  * address_country : string
  * address_city : string
  * address_street : string
  * address_house : string
  * address_apartment : string
  * address_house : string
  * timeslot_id : string
  * status_id <<FK>>
}

entity items {
  * id : uuid <<PK>>
  * basket_id <<FK>>
  * quantity integer
  * good_id uuid
}

entity time_slots {
  * id : uuid <<PK>>
  * name string
  * start integer
  * end integer
}

entity statuses {
  * id : integer <<PK>>
  * name : string
}

baskets ||-  items
baskets }o--|| statuses
baskets }o--|| time_slots

items ||-  goods
```

