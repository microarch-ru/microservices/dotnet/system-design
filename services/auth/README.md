[[_TOC_]]

# Auth
Отвечает за аутентификацию и авторизацию пользователей

## Use case diagram
Диаграмма вариантов использования показывает, какой функционал разрабатываемой программной системы доступен каждой группе пользователей.

```plantuml
left to right direction
skinparam packageStyle rectangle

actor Покупатель as client

actor "BFF (API Gateway)" as bff << Система >>

rectangle Auth {
  usecase (UC-1 Аутентифицироваться) as UC1
  usecase (UC-2 Выйти) as UC2
  usecase (UC-3 Получить cert.pub) as UC3
  
  url of UC1 is [[use-cases/uc-1.md]]
  url of UC2 is [[use-cases/uc-2.md]]
  url of UC3 is [[use-cases/uc-3.md]]  
}

client --> UC1
client -->UC2
UC3 <-- bff
```

**Use cases**
- [UC-1](use-cases/uc-1.md) Аутентифицироваться.
- [UC-2](use-cases/uc-2.md) Выйти.
- [UC-3](use-cases/uc-3.md) Получить cert.pub.

## Container diagram
Диаграмма контейнеров показывает высокоуровневую архитектуру программного обеспечения и то, как в ней распределяются обязанности. Она также показывает основные используемые технологии и то, как контейнеры взаимодействуют друг с другом. Это простая схема высокого уровня, ориентированная на технологии, которая одинаково полезна как для разработчиков программного обеспечения, так и для персонала службы поддержки и эксплуатации.

```plantuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml
' Components
!define actors https://gitlab.com/microarch-ru/microservices/system-design/-/raw/main/containers/actors
!define frontends https://gitlab.com/microarch-ru/microservices/system-design/-/raw/main/containers/frontends  
!define services https://gitlab.com/microarch-ru/microservices/system-design/-/raw/main/containers/services

skinparam wrapWidth 200
skinparam maxMessageSize 200

LAYOUT_TOP_DOWN()
' LAYOUT_WITH_LEGEND()

!include actors/customer.puml

System_Boundary(boundary, "Auth") {
' Shop
!include frontends/shop/web_app.puml
!include frontends/shop/gateway.puml
Rel(customer, shop_web_app, "Использует", "Sync, HTTPS")

' Services
!include services/auth/normal.puml
!include services/auth/db.puml

Rel(shop_web_app, auth, "Аутентифициуется, получает JWT токен", "Sync, HTTPS")
Rel_R(shop_bff, auth, "Получает cert.pub раз в 12 часов", "Sync, HTTPS")
}
```

## Component diagram
Диаграмма компонентов показывает, из каких «компонентов» состоит контейнер, что представляет собой каждый из этих компонентов, его обязанности, технологии и детали реализации.

> Не применимо, так как мы используем коробочное решение Keycloak.