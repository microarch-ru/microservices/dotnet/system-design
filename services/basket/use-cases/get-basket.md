# Получить состав корзины

```plantuml
actor "Покупатель" as customer
participant "shop" as shop_app
participant "shop BFF" as shop_bff
participant "basket" as basket

customer -> shop_app: Получить состав корзины
shop_app -> shop_bff: Получить состав корзины
note over shop_bff: [GET] /api/v1/baskets/{basketId}/items

shop_bff -> basket: Проксирует запрос
note over basket: [GET] /api/v1/baskets/{basketId}/items

shop_app <--  basket: 200 OK
note over shop_app: Состав корзины и параметры доставки
customer <--  shop_app: Корзина
```