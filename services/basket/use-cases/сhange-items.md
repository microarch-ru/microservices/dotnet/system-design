# Изменить состав корзины

```plantuml
actor "Покупатель" as customer
participant "shop" as shop_app
participant "shop BFF" as shop_bff
participant "basket" as basket

customer -> shop_app: Изменить состав корзины
shop_app -> shop_bff: Изменить состав корзины
note over shop_bff: [POST] /api/v1/baskets/{basketId}/items/change

shop_bff -> basket: Проксирует запрос
note over basket: [POST] /api/v1/baskets/{basketId}/items/change

shop_app <--  basket: 200 OK
note over shop_app: Корзина изменена
customer <--  shop_app: Корзина
```