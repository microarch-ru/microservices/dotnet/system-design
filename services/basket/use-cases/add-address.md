# Добавить адрес доставки

```plantuml
actor "Покупатель" as customer
participant "shop" as shop_app
participant "shop BFF" as shop_bff
participant "basket" as basket

customer -> shop_app: Добавить адрес доставки
shop_app -> shop_bff: Добавить адрес доставки
note over shop_bff: [POST] /api/v1/baskets/{basketId}/address/add

shop_bff -> basket: Проксирует запрос
note over basket: [POST] /api/v1/baskets/{basketId}/address/add

shop_app <--  basket: 200 OK
note over shop_app: Адрес добавлен к корзине
customer <--  shop_app: Корзина
```