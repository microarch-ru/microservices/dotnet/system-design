[[_TOC_]]

# О проекте

<div style="background: #E7F0FF; border: 1px dashed #4786FF; border-radius: 2px; padding: 1rem; margin-bottom: 1rem">
 <span style="font-weight: bold; color: #4786FF; padding-right: 0.5rem;">ⓘ Инфо</span>
<p>
Данный проект является документацией к серии курсов по проектированию и разработке микросервисной системы
</p>
<p>
  <ul>
    <li>
      <a href="https://microarch.ru/">Курс "Event Storming и паттерны микросервисной архитектуры"</a>
    </li>
    <li>
      <a href="https://microarch.ru/courses/hexagonal-architecture">Курс "Архитектура микросервиса"</a>
    </li>
  </ul>
</p>
</div>

# Проблема

От CEO нам была поставлена цель - спроектировать интернет магазин с использованием микросервисной архитектуры.
Наша задача: провести декомпозицию системы на микросервисы и реализовать их в коде.

# Event Storming

Для выявления границ микросервисов мы приняли решение использовать практику Event Storming.
После проведения Event Storming мы выделили следующие Bounded Context:
![Карта контекстов](img/es.jpg)

# Решение

## Context diagram

Схема системного контекста является хорошей отправной точкой для построения диаграмм и документирования программной системы, позволяя вам посмотреть со стороны и увидеть общую картину. Нарисуйте схему, показывающую вашу систему в виде прямоугольника в центре, окруженного её пользователями и другими системами, с которыми она взаимодействует.

Детали здесь не важны, так как это вид, показывающий общую картину системного ландшафта. Основное внимание следует уделять людям (актерам, ролям, персонажам и т.д.) и программным системам, а не технологиям, протоколам и другим деталям низкого уровня. Это своего рода схема, которую вы могли бы показать нетехническим людям.

```plantuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Context.puml

skinparam wrapWidth 200
skinparam maxMessageSize 200

LAYOUT_TOP_DOWN()
' LAYOUT_WITH_LEGEND()

Person(customer, Покупатель, "Совершает покупки")

System_Boundary(shop_boundary, "Shop") {
Person(manager, Менеджер, "Управляет интернет магазином")
Person(courier, Курьер, "Доставляет заказ")

' Shop
System(shop, "Shop", "Интернет магазин")
Rel_D(customer, shop, "Делает покупки")
Rel_L(manager, shop, "Управляет магазином")
Rel(shop, courier, "Назначает заказ")

' Auth
System_Ext(auth, "Auth", "Сервер аутентификации")
Rel_L(shop, auth, "Использует")
Rel_L(customer, auth, "Авторизуется")
}
```

## Use case diagram

```plantuml
left to right direction
skinparam packageStyle rectangle

actor Покупатель as client
actor Менеджер as manager
actor Курьер as courier

rectangle Shop {
  ' auth
  usecase (Аутентифицироваться) as UC1
  usecase (Выйти) as UC2
  
  ' catalog
  usecase (Просмотреть каталог товаров) as UC3
  usecase (Просмотреть карточку товара) as UC4
  
  ' delivery
  usecase (Получить статус заказа) as UC5
  usecase (Отменить заказ) as UC6
  usecase (Доставить заказ) as UC7
  usecase (Назначить заказ) as UC8
  
  ' basket
  usecase (Изменить состав корзины) as UC9
  usecase (Добавить адрес доставки) as UC10
  usecase (Просмотреть корзину) as UC11
  usecase (Оформить корзину) as UC12
}

client --> UC1
UC1 <-- manager
UC1 <--courier

client --> UC2
UC2 <-- manager
UC2 <--courier

client --> UC3
client --> UC4

client --> UC5
UC5 <-- manager
UC6 <-- manager

UC7 <--courier

client --> UC9
client --> UC10
client --> UC11
client --> UC12
```

## Container diagrams

### Frontends + BFFs

Данная диаграмма описывает взаимодействие пользователей и ИТ систем.
Микросервисы сгруппированы.

```plantuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml
' Components
!define actors https://GitLab.com/microarch-ru/microservices/system-design/-/raw/main/containers/actors
!define frontends https://GitLab.com/microarch-ru/microservices/system-design/-/raw/main/containers/frontends  
!define services https://GitLab.com/microarch-ru/microservices/system-design/-/raw/main/containers/services

skinparam wrapWidth 200
skinparam maxMessageSize 200

LAYOUT_TOP_DOWN()
' LAYOUT_WITH_LEGEND()

Person(customer, Покупатель, "Совершает покупки")
Person(manager, Менеджер, "Управляет интернет магазином")
Person(courier, Курьер, "Доставляет заказ")
System_Boundary(shop, "Shop") {

' Shop
Container(shop_app, "Shop", "Web, React", "Витрина интернет магазина")
Container(shop_bff, "Shop BFF", "Api Gateway, KrakenD", "Маршрутизация трафика c web приложения shop, аутентификацяи, авторизация")
Rel(shop_app, shop_bff, "Использует", "Sync, HTTPS")
Rel(customer, shop_app, "Делает покупки", "Sync, HTTPS")

' Backoffice
Container(backoffice_web_app, "Backoffice", "Web, React", "Панель управления интернет магазином")  
Container(backoffice_bff, "Backoffice BFF", "Api Gateway, KrakenD", "Маршрутизация трафика, аутентификацяи, авторизация")
Rel(backoffice_web_app, backoffice_bff, "Использует", "Sync, HTTPS")
Rel(manager, backoffice_web_app, "Управляет интернет магазином", "Sync, HTTPS")

' Сourier App
Container(courier_app, "Courier App", "Mobile, React Native", "Приложение курьера")  
Container(courier_bff, "Courier BFF", "Api Gateway, KrakenD", "Маршрутизация трафика, аутентификацяи, авторизация")
Rel(courier_app, courier_bff, "Изменить статус доставки", "Sync, HTTPS")
Rel(courier, courier_app, "Изменить статус доставки", "Sync, HTTPS")

Container(microservices, "Microservices", "Docker", "Группа микросервисов")
Rel(shop_bff, microservices, "Использует", "Sync, HTTPS")
Rel(backoffice_bff, microservices, "Использует", "Sync, HTTPS")
Rel(courier_bff, microservices, "Использует", "Sync, HTTPS")
}
```

<!-- ### Auth
Данная диаграмма описывает взаимодействие пользователей и ИТ систем.
Микросервисы сгруппированы.
```plantuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml
' Components
!define actors https://GitLab.com/microarch-ru/microservices/system-design/-/raw/main/containers/actors
!define frontends https://GitLab.com/microarch-ru/microservices/system-design/-/raw/main/containers/frontends  
!define services https://GitLab.com/microarch-ru/microservices/system-design/-/raw/main/containers/services

skinparam wrapWidth 200
skinparam maxMessageSize 200

LAYOUT_TOP_DOWN()
' LAYOUT_WITH_LEGEND()

Person(customer, Покупатель, "Совершает покупки")
Person(manager, Менеджер, "Управляет интернет магазином")
Person(courier, Курьер, "Доставляет заказ")
System_Boundary(shop, "Shop") {
' Shop
Container(shop_app, "Shop", "Web, React", "Витрина интернет магазина")
Container(shop_bff, "Shop BFF", "Api Gateway, KrakenD", "Маршрутизация трафика c web приложения shop, аутентификацяи, авторизация")
Rel(shop_app, shop_bff, "Использует", "Sync, HTTPS")
Rel(customer, shop_app, "Делает покупки", "Sync, HTTPS")

' Backoffice
Container(backoffice_web_app, "Backoffice", "Web, React", "Панель управления интернет магазином")  
Container(backoffice_bff, "Backoffice BFF", "Api Gateway, KrakenD", "Маршрутизация трафика, аутентификацяи, авторизация")
Rel(backoffice_web_app, backoffice_bff, "Использует", "Sync, HTTPS")
Rel(manager, backoffice_web_app, "Управляет интернет магазином", "Sync, HTTPS")

' Сourier App
Container(courier_app, "Courier App", "Mobile, React Native", "Приложение курьера")  
Container(courier_bff, "Courier BFF", "Api Gateway, KrakenD", "Маршрутизация трафика, аутентификацяи, авторизация")
Rel(courier_app, courier_bff, "Изменить статус доставки", "Sync, HTTPS")
Rel(courier, courier_app, "Изменить статус доставки", "Sync, HTTPS")

' Services
Container_Ext(auth, "Auth", "Keycloak, Java", "Сервер аутентификации")
Rel(shop_app, auth, "Аутентифициуется", "Sync, HTTPS")
Rel(backoffice_web_app, auth, "Аутентифициуется", "Sync, HTTPS")
Rel(courier_app, auth, "Аутентифициуется", "Sync, HTTPS")
}
``` -->

### Микросервисы

```plantuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml
' Components
!define actors https://GitLab.com/microarch-ru/microservices/system-design/-/raw/main/containers/actors
!define frontends https://GitLab.com/microarch-ru/microservices/system-design/-/raw/main/containers/frontends  
!define services https://GitLab.com/microarch-ru/microservices/system-design/-/raw/main/containers/services

skinparam wrapWidth 200
skinparam maxMessageSize 200

LAYOUT_TOP_DOWN()
' LAYOUT_WITH_LEGEND()

!include actors/customer.puml
!include actors/manager.puml
!include actors/courier.puml

System_Boundary(microservices, "Microservices") {
  ' !include services/auth/ext.puml
  ' Rel_R(customer, auth_ext, "Аутентифицироваться", "Sync, HTTP")

  !include services/basket/normal.puml
  Rel_R(customer, basket, "Изменить состав корзины", "Sync, HTTPS")

  !include services/discount/normal.puml
  Rel_R(basket, discount, "Проверить наличие скидки", "Sync, gRPC")

  !include services/catalog/normal.puml
  ' Rel(catalog, basket, "Товар добавлен/изменен", "Async, RabbitMQ")
  Rel(basket, catalog, "Получить данные о товаре", "Sync, gRPC")
  Rel_R(customer, catalog, "Найти и выбрать товар", "Sync, HTTPS")
  Rel_U(manager, catalog, "Добавить/Изменить товар", "Sync, HTTPS")

  !include services/delivery/normal.puml
  Rel(basket, delivery, "Корзина оформлена", "Async, RabbitMQ")
  Rel_U(manager, delivery, "Получить статус доставки", "Sync, HTTPS")
  Rel_R(delivery,courier,"Назначает заказ")
}
```

# Системы

## Backend

| Subdomain                      | Тип Subdomain | Команда  | Микросервис                   | Репозиторий                                                                                                                              | Что делает                                                    |
| ------------------------------ | ------------- | -------- | ----------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------- |
| Продуктовый каталог            | Core          | Alpha    | [catalog](services/catalog)   | [C#](https://GitLab.com/microarch-ru/microservices/dotnet/catalog)                                                                       | Каталог, Товары                                               |
| Процесс оформления заказа      | Core          | Beta     | [basket](services/basket)     | [C#](https://GitLab.com/microarch-ru/microservices/dotnet/basket), [Go](https://GitLab.com/microarch-ru/microservices/go/basket)         | Оформление заказа                                             |
| Процесс сборки и доставки      | Core          | Gamma    | [delivery](services/delivery) | [C#](https://GitLab.com/microarch-ru/microservices/dotnet/delivery), [Go](https://GitLab.com/microarch-ru/microservices/dotnet/delivery) | Курьеры, процесс досставки                                    |
| Скидки и акции                 | Supporting    | Omega    | [discount](services/discount) | [C#](https://GitLab.com/microarch-ru/microservices/dotnet/discount)                                                                      | Предоставляет скидку, если состав корзины подпадает под акцию |
| Безопасность, контроль доступа | Generic       | Platform | [auth](services/auth)         | [Box (open source), Java](https://GitLab.com/microarch-ru/microservices/dotnet/auth)                                                     | Регистрация, аутентификация, авторизация                      |

## Frontend

| Назначение                           | Команда             | Приложение                         | Репозиторий                                                                          | Что делает                        |
| ------------------------------------ | ------------------- | ---------------------------------- | ------------------------------------------------------------------------------------ | --------------------------------- |
| Витрина интернет магазина            | Alpha, Beta         | [shop](front-end/shop)             | [No-code](https://GitLab.com/microarch-ru/microservices/dotnet/front-end/shop)       | Каталог, карточка товара, корзина |
| Панель управления интернет магазином | Alpha, Gamma, Delta | [backoffice](front-end/backoffice) | [No-code](https://GitLab.com/microarch-ru/microservices/dotnet/front-end/backoffice) | Управление каталогом, заказами    |
| Приложение курьера                   | Gamma               | [courier](front-end/courier)       | [No-code](https://GitLab.com/microarch-ru/microservices/dotnet/front-end/courier)    | Принятие заказов                  |
