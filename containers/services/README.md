```plantuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

' Components 
!define services https://gitlab.com/microarch-ru/microservices/system-design/-/raw/main/containers/services

!include services/auth/normal.puml
!include services/basket/normal.puml
!include services/catalog/normal.puml
!include services/discount/normal.puml
!include services/delivery/normal.puml
!include services/geo/normal.puml
!include services/notification/normal.puml
```